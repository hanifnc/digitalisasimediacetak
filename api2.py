import flask
from flask import request, jsonify
import cv2
from skimage.filters import threshold_local, threshold_sauvola, threshold_niblack, threshold_otsu
import numpy as np
import imutils
from pythonRLSA import rlsa
import math
from tesserocr import PyTessBaseAPI
from PIL import Image
from skimage import io
import os
from pdf2image import convert_from_path, convert_from_bytes
import urllib
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True


def content_titles_separator(image):
    im = image.copy()
    try:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    except:
        gray = image    
    
    
    (thresh, binary) = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    (contours, _) = cv2.findContours(~binary,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
    
    for contour in contours:
        [x,y,w,h] = cv2.boundingRect(contour)
        cv2.rectangle(image, (x,y), (x+w,y+h), (0, 255, 0), 1)
    
    mask = np.ones(image.shape[:2], dtype="uint8") * 255 
    (contours, _) = cv2.findContours(~binary,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    heights = [cv2.boundingRect(contour)[3] for contour in contours]
    avgheight = sum(heights)/len(heights)

    for c in contours:
        [x,y,w,h] = cv2.boundingRect(c)
        if h > 2*avgheight:
            cv2.drawContours(mask, [c], -1, 0, -1)
    
    value = max(math.ceil(x/100),math.ceil(y/100))+20
    mask = rlsa.rlsa(mask, True, False, value)
    
    (contours, _) = cv2.findContours(~mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
    mask2 = np.ones(image.shape, dtype="uint8") * 255 

    for contour in contours:
        [x, y, w, h] = cv2.boundingRect(contour)
        if w > 0.50*im.shape[1]: 
            title = im[y: y+h, x: x+w] 
            mask2[y: y+h, x: x+w] = title 
            im[y: y+h, x: x+w] = 255

    return(mask2,im)

def sauvola_thresholding(image):
    try:
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    except:
        image_gray = image
    ts = threshold_sauvola(image_gray)
    sauvola = (image_gray > ts).astype("uint8") * 255
    return(sauvola)

def image_from_nparray(nparray):
    img = Image.fromarray(nparray)
    return(img)

def ocr_reader(image):
    
    with PyTessBaseAPI(path="./tessadata/tessdata-master/.", lang= "ind") as api:
        api.SetImage(image)
        data = api.GetUTF8Text()
       
       
       
        wordConfidence = np.average(api.AllWordConfidences())

    return(data, wordConfidence)

def content_post_processing(text):
    context = text[0]
    sentences = context.split("\n")
    paraghrap = ""
    before = ""

    for sentence in sentences:
        if sentence[-1:]== "-":
            sentence = sentence[0:-1]
        else:
            sentence += " "

        if sentence != " ":
            paraghrap += sentence
        elif before !=" ":
            paraghrap += "\n"
        else:
            paraghrap = paraghrap
        
        before = sentence

    return(paraghrap)

def title_post_processing(text):
    title = text[0]
    title = title.replace("\n"," ")
    return (title)

def ocr_process(image):
    titles_content = content_titles_separator(image)
    a = np.percentile(titles_content, 10)
    b = np.percentile(titles_content, 90)
    c = b-a
    print(a)
    print(b)

    if(c > 200):
        titles_data = titles_content[0]
        content_data = titles_content[1]

    else:
        titles_data = sauvola_thresholding(titles_content[0])
        content_data = sauvola_thresholding(titles_content[1])

    title = title_post_processing(ocr_reader(image_from_nparray(titles_data)))
    content = content_post_processing(ocr_reader(image_from_nparray(content_data)))   

    return(title,content)

def ocr_process_pdf(url):
    pdf_file = requests.get(url)
    images = convert_from_bytes(pdf_file.content)
    hasil ={}
    print(len(images))
    x = 1
    for image in images:
        cvformat = np.array(image)  
        cvformat = cvformat[:, :, ::-1].copy()
        hasil["halaman "+str(x)] = ocr_process_img(cvformat)
        x-=-1
    results = hasil
    
    return results

def ocr_process_img(imageFile):
    hasil = ocr_process(imageFile)
    results = {"title":hasil[0], "content":hasil[1],}
    return results


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Welcome to OCR Reader for Newspaper</h1>
<p> coba cek /api/reader?url="url gambar mu"</p>'''


@app.route('/api/reader', methods=['GET'])
def api_id():
    if 'url' in request.args:
        url = str(request.args['url'])
    else:
        return "Error: No id field provided. Please specify an id."

    try:
        url = url.replace(" ", "%20")
        file_ext = os.path.splitext(url)[-1]
        print(url)
        print(file_ext)
        
    
        if file_ext == ".pdf":
            results = ocr_process_pdf(url)

        else:
            image = io.imread(url)
            print(file_ext)
            results = ocr_process_img(image)
        
    except:
        results="ERROR"

    return jsonify(results)

app.run()